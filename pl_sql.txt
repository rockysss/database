
create table vacancies (
    id number primary key NOT NULL,
    d_id number ,
    vacancies  number,
    salary  number
);


DECLARE
    total_employee NUMBER;
    dpt_id number;
    vac_id number := 1;
BEGIN
    for i in 1..27 loop
    dpt_id := i*10;
    SELECT Count(*)
    INTO   total_employee
    FROM   HR.EMPLOYEES e
           join HR.DEPARTMENTS d
             ON e.department_id = d.department_id
    WHERE  e.department_id = dpt_id;
    IF total_employee < 50 THEN
        if vac_id = 0 then
            vac_id := vac_id+1;
            end if;
      dbms_output.Put_line ('vacancies in the department '|| dpt_id|| ' is ' || To_char(50-total_employee));
      insert into vacancies values(vac_id,dpt_id,(50-total_employee),5000);
      vac_id:= vac_id+1;
    ELSE
      dbms_output.Put_line ('There are no vacancies in department_id');
    END IF;
    end loop;
END;

select * from vacancies;