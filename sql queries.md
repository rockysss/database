##### 1. list of consultants having more than one submission
	
	select * from consultants c where c.c_id in (
		select cid from submission group by cid 
		having count(cid) > 1
	);

--------------------------------------------------------
##### 2. list all the interviews of consultants
	
	select c.name, c.id, i.inter_id , i.inters_date, i.intere_date, i.status, i.remark from interview i
	inner join submission s on i.sub_id = s.sub_id inner join consultant c on 
	c.id = s.consultant_id;

-------------------------------------------------------
##### 3. list all PO of marketers
--->
	select emp_id, emp_name, emp_company, emp_email from employee e
	inner join submission s on e.emp_id = s.emp_id inner join 
	purchase_order p on s.sumission_id = p.submission_id; 


-------------------------------------------------------
##### 4. unique vendor company name for which client location is the same

	select distinct vendor_id, vendor_city, vandor_name from vendor v
	inner join submission on v.vendor_location = submission.location;


-------------------------------------------------------
##### 5. count of consultants which submitted in the same city

	select c.consultant_id,  c.consultant_name, count(c.coun_id) from consultant c 
	inner join submission s.location = c.location;

	 


--------------------------------------------------------
##### 6. name of consultant and client who have been submitted on the same vendor


	select c.name, cl.name, s.id, s.name from  consultant c, client cl, submission s where c.client_id = cl.client_id AND 
	s.id = c.consu_id;

	






